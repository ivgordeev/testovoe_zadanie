import Vue from 'vue';
import VueRouter from 'vue-router';
import NotesPage from './components/NotesPage';
import ArchivePage from './components/ArchivePage';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [{
    path: "/", component: NotesPage,
  }, {
    path: "/archive", component: ArchivePage,
  }]
});
export default router;
