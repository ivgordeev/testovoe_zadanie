export default {
  methods: {
    changeStatus() {
      const status = this.status === 'new' ? 'picked' : 'new';
      this.editNoteById({id: this.id, data: {status}});
    },
    changeArchive() {
      const status = this.status === 'archive' ? 'new' : 'archive';
      this.editNoteById({id: this.id, data: {status}});
    }
  },
  computed: {
    getStar() {
      return this.note.status === 'new' ? 'star_border' : 'star'
    },
    onArchive() {
      return this.note.status === 'archive' ? 'unarchive' : 'archive'
    }
  },


}
