import Vue from 'vue';
import Vuex from 'vuex';
import NotesStore from './modules/notes.store';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    NotesStore,
  },
});
export default store;
