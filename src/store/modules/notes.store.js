const state = {
  notes: [],
  search: '',
};
const mutations = {
  editNoteById(state, {id, data}) {
    const note = state.notes.find(item => item.id === id);
    for (const key of Object.keys(data)) {
      note[key] = data[key];
    }
    localStorage.setItem('notes', JSON.stringify(state.notes));
  },
  deleteNoteById(state, id) {
    state.notes.find((item, index) => {
      if (item.id === id) {
        return state.notes.splice(index, 1);
      }
    });
    localStorage.setItem('notes', JSON.stringify(state.notes));
  },
  addNote(state, data) {
    data.id = Math.random().toString(36).substr(2, 9);
    data.date = Date.now();
    state.notes.push(data);
    localStorage.setItem('notes', JSON.stringify(state.notes));
  },
  setSearch(state, value) {
    state.search = value;
  },
  getNotesFromLocalStorage(state) {
    let notes = localStorage.getItem('notes');
    if (!notes) return;
    try {
      notes = JSON.parse(notes);
    }
    catch (e) {
      return localStorage.removeItem(notes);
    }
    state.notes = notes;
  }
};
const getters = {
  getNotes: (state, getters) => status => {
    let notes = state.notes;
    if (getters.getSearch) {
      const query = new RegExp(getters.getSearch, 'ig');
      notes = notes.filter(item => query.test(item.title) || query.test(item.desk));
      if (!notes) return [];
    }
    notes = notes.filter(item => item.status === status);
    notes = notes.slice();
    notes.sort((a, b) => {
      if (a.date > b.date) return -1;
      if (a.date < b.date) return 1;
    });
    return notes;
  },
  getSearch(state) {
    return state.search;
  }
};

export default {
  state,
  getters,
  mutations
}
